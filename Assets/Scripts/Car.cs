﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Car : MonoBehaviour
{
    #region Variables
    public static Car instance;

    [SerializeField] PathCreator pathCreator;
    //[SerializeField] GameObject arrow;
    [SerializeField] float crashForce;

    Queue<Vector3> pathPoints;
    NavMeshAgent agent;
    Rigidbody rb;

    bool isStarted;

    private Transform spawnTransform;

    private Transform currentTransform;

    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        instance = this;

        string name = this.transform.name;
        pathCreator = GameObject.Find(name[1] + "PathCreator(Clone)").GetComponent<PathCreator>();


        currentTransform = GetComponent<Transform>();
        spawnTransform = currentTransform;
        rb = GetComponent<Rigidbody>();
        pathPoints = new Queue<Vector3>();
        agent = GetComponent<NavMeshAgent>();
        pathCreator.OnNewPathCreated += SetPoints;
    }

    private void Update()
    {
        if (isStarted)
        {
            UpdateDestionation();
            /*if (agent.hasPath)
                agent.speed = (agent.remainingDistance > 1f) ? 60 : 2;*/
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Car") || collision.transform.CompareTag("Obstacle"))
        {
            FxManager.Instance.Crash(new Vector3(collision.contacts[0].point.x, collision.contacts[0].point.y + 3f, collision.contacts[0].point.z));
            Crash();
        }
    }

    #endregion

    #region Other Methods

    public void SetPoints(IEnumerable<Vector3> points)
    {
        pathPoints = new Queue<Vector3>(points);
    }

    bool CanSetDestionation()
    {
        if (pathPoints.Count == 0)
            return false;
        if (agent.hasPath == false || agent.remainingDistance < .85f)
            return true;
        return false;
    }

    void UpdateDestionation()
    {
        if (CanSetDestionation())
            agent.SetDestination(pathPoints.Dequeue());
    }

    public void StartToMoving()
    {
        rb.isKinematic = false;
        isStarted = true;
        FxManager.Instance.spawnBlinker(this.transform, this.gameObject.name);
    }

    public void Respawn()
    {
        //agent.ResetPath();
        //agent.isStopped = true;
        agent.enabled = false;
        rb.AddForce((crashForce * transform.up) + (-transform.forward * crashForce / 2));
        Destroy(this);
        Debug.Log(currentTransform.position);
        //Vector3 newPos = new Vector3(spawnTransform.position.x, spawnTransform.position.y, spawnTransform.position.z);
        //currentTransform.position = newPos;

        currentTransform = spawnTransform;



        
        //rb.useGravity = true;


        Vector3 newPos = new Vector3();
        newPos = spawnTransform.position;
        currentTransform.position = newPos;

        //agent.enabled = true;
        //rb.useGravity = false;

        Debug.Log(currentTransform.position);
    }

    public void Crash()
    {
        //agent.Stop();
        agent.isStopped = true;
        agent.enabled = false;

        /*rb.useGravity = true;
        rb.isKinematic = false;*/

        rb.AddForce((crashForce * transform.up) + (-transform.forward * crashForce / 2));
        rb.AddTorque((transform.forward + transform.up)* Random.Range(crashForce-50, crashForce));
        if (PlayerPrefs.GetInt("Vibration") != 0)
        {
            Handheld.Vibrate();
        }   
        SoundScript.Instance.PlaySound("CrashSound");
        StartCoroutine(Ggg());
    }

    IEnumerator Ggg()
    {
        yield return new WaitForSecondsRealtime(1);
        rb.useGravity = true;
        Destroy(this);

    }

    public void CarParked()
    {
        agent.isStopped = true;
        rb.isKinematic = true;
        Destroy(this);
    }
    #endregion
}
