﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;


public class GoogleAdsScript : MonoBehaviour
{
    private string bannerID;
    private string interstitialID;
    private string rewardedID;

    private BannerView bannerView;
    private InterstitialAd interstitial;
    private RewardedAd rewardedAd;


    private static GoogleAdsScript instance;
    public static GoogleAdsScript Instance
    {
        get
        {
            if(instance==null)
            {
                instance = GameObject.FindObjectOfType<GoogleAdsScript>();
            }
            return instance;
        }
    }

    void Start()
    {
        bannerID = AdsScript.Instance.bannerID;
        interstitialID = AdsScript.Instance.interstitialID;
        rewardedID = AdsScript.Instance.rewardedID;

        MobileAds.Initialize(initStatus => { });

        GenerateInterstitalRequest();
        GenerateRewardedRequest();
    }

    public void RequestBanner()
    {
        if (bannerView != null)
            bannerView.Destroy();

        this.bannerView = new BannerView(bannerID, AdSize.SmartBanner, AdPosition.Bottom);

        this.bannerView.LoadAd(GenerateBannerRequest()); ;
    }

    public void RequestBannerForScript(int position)
    {
        if (bannerView != null)
            bannerView.Destroy();

        if (position == 0)
            this.bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.TopLeft);
        else if (position == 1)
            this.bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.Top);
        else if (position == 2)
            this.bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.TopRight);
        else if (position == 3)
            this.bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.BottomLeft);
        else if (position == 4)
            this.bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.Bottom);
        else if (position == 5)
            this.bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.BottomRight);

        this.bannerView.LoadAd(GenerateBannerRequest());
    }

    public AdRequest GenerateBannerRequest()
    {
        this.bannerView.OnAdLoaded += this.HandleOnAdLoaded;
        this.bannerView.OnAdFailedToLoad += this.HandleOnAdFailedToLoad;
        this.bannerView.OnAdOpening += this.HandleOnAdOpened;
        this.bannerView.OnAdClosed += this.HandleOnAdClosed;
        this.bannerView.OnAdLeavingApplication += this.HandleOnAdLeavingApplication;

        AdRequest request = new AdRequest.Builder().Build();
        return request;
    }

    public void ShowBanner()
    {
        this.bannerView.Show();
    }

    public void HideBanner()
    {
        this.bannerView.Hide();
    }

    public void DestroyBanner()
    {
        this.bannerView.Destroy();
    }

    public void RequestInterstitialAd()
    {
        /*if (interstitial != null)
            interstitial.Destroy();*/

        if( PlayerPrefs.GetInt("AdsPurchased", 0) == 0)
        {
            if (this.interstitial.IsLoaded())
            {
                this.interstitial.Show();
                GenerateInterstitalRequest();
            }
        }        
    }


    public void RequestRewardedAd()
    {
        if (PlayerPrefs.GetInt("AdsPurchased", 0) == 0)
        {
            if (this.rewardedAd.IsLoaded())
            {
                this.rewardedAd.Show();
                GenerateRewardedRequest();
            }

        }           
    }

    public void GenerateInterstitalRequest()
    {
        /*if (interstitial != null)
            interstitial.Destroy();*/

        this.interstitial = new InterstitialAd(interstitialID);

        this.interstitial.OnAdLoaded += HandleOnInterstitialLoaded;
        this.interstitial.OnAdFailedToLoad += HandleOnInterstitialFailedToLoad;
        this.interstitial.OnAdOpening += HandleOnInterstitialOpened;
        this.interstitial.OnAdClosed += HandleOnInterstitialClosed;
        this.interstitial.OnAdLeavingApplication += HandleOnInterstitialLeavingApplication;

        AdRequest request = new AdRequest.Builder().Build();
        this.interstitial.LoadAd(request);
    }

    public bool RequestInterstitialForScript()
    {
        if (this.interstitial.IsLoaded())
        {
            interstitial.Show();
            GenerateInterstitalRequest();
            return true;
        }      
        else
        {
            return false;
        }
    }
    public void GenerateRewardedRequest()
    {
        this.rewardedAd = new RewardedAd(rewardedID);

        this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
        this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
        this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
        this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;

        AdRequest request = new AdRequest.Builder().Build();
        this.rewardedAd.LoadAd(request);
    }

    public bool RequestRewardedForScript()
    {
        
        if (this.rewardedAd.IsLoaded())
        {
            this.rewardedAd.Show();
            GenerateRewardedRequest();
            return true;
        }          
        else
        {
            return false;
        }
    }


    #region Events

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }
    
    //INTERSTITIAL
    public void HandleOnInterstitialLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
    }

    public void HandleOnInterstitialOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnInterstitialClosed(object sender, EventArgs args)
    {
        SceneController.Instance.NextLevel();
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnInterstitialLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }

    //Rewarded
    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdLoaded event received");
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToLoad event received with message: "
                             + args.Message);
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdOpening event received");
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToShow event received with message: "
                             + args.Message);
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdClosed event received");
    }

    public void HandleUserEarnedReward(object sender, Reward args)
    {
        if(PlayerPrefs.GetInt("FreeCurrency",0)==1)
        {
            GameManager.Instance.RewardFor2x();
            PlayerPrefs.SetInt("FreeCurrency", 0);
            PlayerPrefs.SetInt("StopInterstitialAfter2x", 1);
        }  
        /*if(PlayerPrefs.GetInt("FreeCash", 0) == 1)
        {
            //Do something
            PlayerPrefs.SetInt("FreeCash", 0);
        }*/
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardedAdRewarded event received for "
                        + amount.ToString() + " " + type);
    }
    #endregion
}
