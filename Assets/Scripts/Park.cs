﻿using UnityEngine;

public class Park : MonoBehaviour
{
    #region Variables

    [SerializeField] Car targetCar;
    [SerializeField] GameObject parkedFx;

    [SerializeField] GameObject tempCar;

    #endregion

    #region MonoBehaviour Callbacks
    private void Awake()
    {
        char carColor = this.transform.name[0];
        tempCar = GameObject.Find(PlayerPrefs.GetInt("currentCar").ToString() + this.transform.name[0] + "(Clone)");
        targetCar = tempCar.GetComponent<Car>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == targetCar.transform.name)
        {
            parkedFx.SetActive(true);
            ParkManager.Instance.Parked();
            SoundScript.Instance.PlaySound("ParkedSound");
            this.gameObject.transform.GetChild(3).gameObject.SetActive(true);
            this.gameObject.transform.GetChild(4).gameObject.SetActive(true);
            
            other.gameObject.GetComponent<Car>().Invoke("CarParked",.2f);
            Destroy(this);
        }
    }

    /*public void FixPosition()
    {
        tempCar.transform.position = this.transform.position;
        tempCar.transform.rotation = this.transform.rotation;
    }*/

    #endregion
}
