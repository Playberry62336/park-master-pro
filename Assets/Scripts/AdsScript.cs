﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsScript : MonoBehaviour
{
    [Header("Remote Key name for Analytics")] public string remoteKey;


    [Header("Keys for GoogleAdMob")]
    public string bannerID;
    public string interstitialID;
    public string rewardedID;

    [Header("Keys for Unity")]
    public string unityID;
    public string unityBannerID;
    public string unityInterstitialID;
    public string unityRewardedID;

    [Header("Position for Banner")] public BannerPosition bannerPosition;
    public enum BannerPosition
    {
        topLeft,  
        topCentre, 
        topRight,        
        bottomLeft,       
        bottomCentre,        
        bottomRight
    } 

    private static AdsScript instance;
    public static AdsScript Instance
    {
        get
        {
            if(instance==null)
            {
                instance = GameObject.FindObjectOfType<AdsScript>();
            }
            return instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void ShowBanner()
    {
        if (PlayerPrefs.GetInt("AdsPurchased", 0) == 0)
        {
            if(!UnityAdsScript.Instance.RequestBannerForScript((int)bannerPosition))
            {
                GoogleAdsScript.Instance.RequestBannerForScript((int)bannerPosition);
            }
        }
    }

    public void NextScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void ShowInterstitial()
    {
        if(RemoteSettings.GetBool(remoteKey) == true)
        {
            if(PlayerPrefs.GetInt("AdsPurchased", 0) == 0)
            {
                if (!GoogleAdsScript.Instance.RequestInterstitialForScript())
                {
                    if (!UnityAdsScript.Instance.RequestInterstitialForScript())
                    {
                        Debug.Log("No interstitial available!");
                        GameCanvasManager.instance.CallToast();
                        SceneController.Instance.NextLevel();
                    }
                }
            }   
        }
        else if(RemoteSettings.GetBool(remoteKey) == false)
        {
            SceneController.Instance.NextLevel();
        }
    }

    public void ShowRewarded()
    {
        if(RemoteSettings.GetBool(remoteKey) == true)
        {
            if(PlayerPrefs.GetInt("AdsPurchased", 0) == 0)
            {
                if (!GoogleAdsScript.Instance.RequestRewardedForScript())
                {
                    if (!UnityAdsScript.Instance.RequestRewardedForScript())
                    {
                        Debug.Log("No rewarded available!");
                        GameCanvasManager.instance.CallToast();
                        SceneController.Instance.NextLevel();
                    }
                }
            }      
        }
        else if (RemoteSettings.GetBool(remoteKey) == false)
        {
            SceneController.Instance.NextLevel();
        }
    }
    
    public void FreeCurrencyFunction()
    {
        PlayerPrefs.SetInt("FreeCurrency", 1);
        if (!GoogleAdsScript.Instance.RequestRewardedForScript())
        {
            if (!UnityAdsScript.Instance.RequestRewardedForScript())
            {
                Debug.Log("No rewarded available!");
                GameCanvasManager.instance.CallToast();
            }
        }
    }

}
