﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MoreMountains.NiceVibrations;
using UnityEngine.EventSystems;

public class PathCreator : MonoBehaviour
{


    public static PathCreator Instance;

    #region Variables

    public Action<IEnumerable<Vector3>> OnNewPathCreated = delegate { };

    [SerializeField] private new Camera camera;
    [SerializeField] Material lineMaterial;

    // Drawing
    LineRenderer lineRenderer;
    List<Vector3> points = new List<Vector3>();
    
    // Raycasting
    Ray ray;
    RaycastHit hit;


    //new variables
    private Vector2 startPos;
    private Vector2 mousePos;
    private float distance;

    private Vector3 prevPoint;

    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }


        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.material = lineMaterial;
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            {
                //if (EventSystem.current.IsPointerOverGameObject()) return;
                        
                 if (!GameManager.Instance.isGameStarted)
                        GameManager.Instance.StartGame();
                        points.Clear();
                        lineRenderer.enabled = true;
                        startPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                        
                } 
            
            if (Input.GetMouseButton(0))
            {
            //if (EventSystem.current.IsPointerOverGameObject()) return;
            lineRenderer.enabled = true;
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Raycasting();
            }

            else if (Input.GetMouseButtonUp(0))
            {
                //if (EventSystem.current.IsPointerOverGameObject()) return;

                OnNewPathCreated(points);
                PathManager.Instance.NextPathDrawing();
            }  
}

    #endregion
        #region Other Methods

        float DistanceToLastPoint(Vector3 point)
    {
        if (!points.Any())
            return Mathf.Infinity;

        float distance = Vector3.Distance(points.Last(), point);
        return distance;
    }
   
    void Raycasting()
    {   
        ray = camera.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray,out hit))
        {
            if (hit.transform.CompareTag("Ground") || hit.transform.CompareTag("Car") || hit.transform.CompareTag("RespawnBlinker") && DistanceToLastPoint(hit.point) > 0.25f)
            {
                Vector3 point = new Vector3(hit.point.x, 1.1f, hit.point.z);
                //
                //
                //Debug.Log(point);

                if (prevPoint == point)
                    Debug.Log(" ");

                if (prevPoint!=point)
                {
                    points.Add(point);
                    //Debug.Log("Point added : " + point);
                    prevPoint = point;


                    lineRenderer.positionCount = points.Count;
                    lineRenderer.SetPositions(points.ToArray());

                    CalculateDistance();

                    if (PlayerPrefs.GetInt("Vibration") == 1)
                        MMVibrationManager.TransientHaptic(0.25f, 1f);
                }


        
            }
        }
    }

    private void CalculateDistance()
    {
        distance += (points.Count - (points.Count - 1));
        Debug.Log(Mathf.FloorToInt(distance / 2));

        PlayerPrefs.SetInt("CurrentDistance", PlayerPrefs.GetInt("CurrentDistance") + Mathf.FloorToInt(distance / 2));
    }

    public int ShowDistance()
    {
        return Mathf.FloorToInt(distance / 2);
    }

    #endregion
}
