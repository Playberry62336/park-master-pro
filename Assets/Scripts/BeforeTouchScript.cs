﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BeforeTouchScript : MonoBehaviour
{
    [SerializeField] private new Camera camera;
    Ray ray;
    RaycastHit hit;
    private void Start()
    {
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0) || Input.GetMouseButtonUp(0))
            Raycasting();
    }

    void Raycasting()
    {
        ray = camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.CompareTag("Car"))
            {
                Debug.Log("Hit with " + hit.transform.name + " Detected!");
                PathManager.Instance.SKRA(hit.transform.name);
            }
            else if(hit.transform.CompareTag("RespawnBlinker") && PathManager.Instance.checkPaths())
            {
                Debug.Log("Car respawned!");
                SceneController.Instance.RestartLevel();
                //Car.instance.Respawn();
                //Car.instance.Crash();
            }
        }
    }
}
