﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshBaker : MonoBehaviour
{

    public NavMeshSurface[] navMeshSurfaces;

    void Start()
    {
        navMeshSurfaces[0].BuildNavMesh();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
