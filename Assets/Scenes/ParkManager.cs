﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;
using System.Linq;
using System;

public class ParkManager : MonoBehaviour
{
    #region Variables

    public static ParkManager Instance;

    [SerializeField] Park[] parks;

    int parkedCount;

    #endregion

    #region MonoBehaviour Callbacks

    private void Awake()
    {
        Debug.Log(Application.persistentDataPath);

        int levelNo = int.Parse(SceneManager.GetActiveScene().name);

        string filedata = Application.persistentDataPath + "/DataFiles/Levels.txt";

        //string filedata = "Assets/Resources/TextAssets/Levels.json";

        string line = File.ReadLines(filedata).Skip(levelNo - 1).Take(levelNo).First();
        string[] data = line.Split(' ');

        int noOfCars = int.Parse(data[0]);

        parks = new Park[noOfCars];

        GameObject[] tParks = GameObject.FindGameObjectsWithTag("Park");
        for (int i = 0; i < noOfCars; i++)
        {
            parks[i] = tParks[i].GetComponent<Park>();
        }

        Instance = this;
        parkedCount = 0;
    }

    #endregion

    #region Other Methods

    public void Parked()
    {
        parkedCount++;
        if (parkedCount == parks.Length)
        {
            TakeScreenShot();
            FxManager.Instance.LevelComplete();
            UIManager.Instance.LevelPassed();
            GameManager.Instance.SaveCoin();
            GameCanvasManager.instance.turnRandomButtonVisibility();
            GameObject[] blinkers = GameObject.FindGameObjectsWithTag("RespawnBlinker");

            //Debug.Log(PathCreator.Instance.ShowDistance());

            foreach(GameObject b in blinkers)
            {
                Destroy(b);
            }
            if(int.Parse(SceneManager.GetActiveScene().name) >= PlayerPrefs.GetInt("UnlockedLevel"))
            {
                if(PlayerPrefs.GetInt("UnlockedLevel") < SceneManager.sceneCountInBuildSettings - 1)
                {
                    PlayerPrefs.SetInt("UnlockedLevel", int.Parse(SceneManager.GetActiveScene().name) + 1);
                }
            }
            //GameCanvasManager.instance.ReadFromFile();
            //GameCanvasManager.instance.ReadData();
        }
    }


    private void TakeScreenShot()
    {
        int resWidth = 135;
        int resHeight = 246;

        Camera camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);

        camera.targetTexture = rt;
        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        camera.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        camera.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        Destroy(rt);
        byte[] bytes = screenShot.EncodeToPNG();
        string filename = Application.persistentDataPath + "/Screenshots/" + SceneManager.GetActiveScene().name + ".png";

        System.IO.File.WriteAllBytes(filename, bytes);
        Debug.Log(string.Format("Took screenshot to: {0}", filename));
    }
    #endregion
}
